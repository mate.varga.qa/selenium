package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.PositionsDetailsKanbanPage;

public class PositionsDetailsKanbanTest extends BaseTest {
    private PositionsDetailsKanbanPage positionsDetailsKanbanPage;

    @BeforeClass
    public void setUpPartnersDetailsPage() {
        loginToWebsite();
        positionsDetailsKanbanPage = new PositionsDetailsKanbanPage(driver);
        PositionsDetailsKanbanPage.positionsButton.click();
        positionsDetailsKanbanPage.firstPosition.click();
        positionsDetailsKanbanPage.positionsKanbanButton.click();
    }
    @Test(groups = {"noNavigateBack"})
    public void testSortKanban() {
        positionsDetailsKanbanPage.sortKanban();
        positionsDetailsKanbanPage.waitForElementToBeVisible(PositionsDetailsKanbanPage.kanbanSortArrow);
        Assert.assertTrue(PositionsDetailsKanbanPage.kanbanSortArrow.isDisplayed(), "Sikertelen kanban rendezés!");
    }
    @Test(groups = {"noNavigateBack"})
    public void testchangeKanbanView() throws InterruptedException {
        positionsDetailsKanbanPage.changeKanbanView();
        Assert.assertTrue(PositionsDetailsKanbanPage.kanbanViewButton.isDisplayed(), "Sikertelen nézetváltás!");
    }
    @Test(groups = {"noNavigateBack"})
    public void testCreateKanbanCandidate() {
        positionsDetailsKanbanPage.createKanbanCandidate();
        positionsDetailsKanbanPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres jelölt létrehozás!"), "Sikertelen jelölt létrehozás!");
        positionsDetailsKanbanPage.toastMessageButton.click();
    }
}
