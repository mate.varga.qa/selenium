package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.CandidatesDetailsPage;

public class CandidatesDetailsTest extends BaseTest {
    private CandidatesDetailsPage candidatesDetailsPage;

    @BeforeClass
    public void setUpPartnersDetailsPage() {
        loginToWebsite();
        candidatesDetailsPage = new CandidatesDetailsPage(driver);
        CandidatesDetailsPage.candidatesButton.click();
        candidatesDetailsPage.firstCandidate.click();
    }
    @Test(priority = 1)
    public void testWriteHistoryMessage() {
        candidatesDetailsPage.writeHistoryMessage("Teszt jegyzet");
        candidatesDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres üzenet létrehozás!"), "Sikertelen üzenet létrehozás!");
    }
    @Test(priority = 2)
    public void testShowFullHistory() throws InterruptedException {
        candidatesDetailsPage.showFullHistory();
        Assert.assertTrue(BasePage.showFullHistory.isDisplayed(), "Sikertelen teljes történet megjelenítés!");
    }
    @Test(priority = 3)
    public void testEditCandidate() {
        candidatesDetailsPage.editCandidate();
        candidatesDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres jelölt módosítás!"), "Sikertelen jelölt módosítás!");
        candidatesDetailsPage.toastMessageButton.click();
        candidatesDetailsPage.revertEditCandidate();
        candidatesDetailsPage.toastMessageButton.click();
    }
    @Test(priority = 4)
    public void testAddCandidateToPosition() throws InterruptedException {
        candidatesDetailsPage.addCandidateToPosition();
        candidatesDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres jelölt pozíció hozzárendelés!!"), "Sikertelen jelölt pozíció hozzárendelés!!");
        candidatesDetailsPage.toastMessageButton.click();
    }
    @Test(priority = 6)
    public void testDetachCandidate() throws InterruptedException {
        candidatesDetailsPage.detachCandidateFromPosition();
        candidatesDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres jelölt pozíció leválasztás!"), "Sikertelen jelölt pozíció leválasztás!");
        candidatesDetailsPage.toastMessageButton.click();
    }
    @Test(priority = 5)
    public void testStatusChange() {
        candidatesDetailsPage.changeStatus();
        candidatesDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres jelölt pozíció módosítás!"), "Sikertelen jelölt pozíció módosítás!");
        candidatesDetailsPage.toastMessageButton.click();
    }
    @Test(priority = 7)
    public void testDeleteCandidate() {
        candidatesDetailsPage.deleteCandidate();
        candidatesDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres jelölt törlés!"), "Sikertelen jelölt törlés!");
    }
}
