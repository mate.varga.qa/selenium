package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.BasedataSourcesPage;

public class BasedataSourcesTest extends BaseTest {
    private BasedataSourcesPage basedataSourcesPage;
    @BeforeClass
    public void setUpBaseDataSourcePage() {
        loginToWebsite();
        basedataSourcesPage = new BasedataSourcesPage(driver);
        BasedataSourcesPage.basedataButton.click();
    }
    @Test(priority = 1)
    public void testSearchSources() throws InterruptedException {
        basedataSourcesPage.searchSource();
        basedataSourcesPage.waitForElementToBeVisible(basedataSourcesPage.sourceFacebook);
        boolean result = basedataSourcesPage.sourceFacebook.isDisplayed();
        Assert.assertTrue(result, "Sikertelen keresés!");
        driver.navigate().refresh();
    }
    @Test(priority = 2)
    public void testSortSource() throws InterruptedException {
        basedataSourcesPage.sortSource();
        Assert.assertTrue(basedataSourcesPage.columnSortAscend.isDisplayed(), "Sikertelen rendezés!");
    }
    @Test(priority = 3)
    public void testColumnMove() throws InterruptedException {
        basedataSourcesPage.columnMove(basedataSourcesPage.columnSort);
        Assert.assertTrue(basedataSourcesPage.columnHeader.isDisplayed(), "Sikertelen oszlop mozgatás!");
        driver.navigate().refresh();
    }
    //Sikeres forrás archiválás!
    @Test(priority = 4)
    public void testEditSource() throws InterruptedException {
        basedataSourcesPage.editSource();
        basedataSourcesPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres forrás modosítása!"), "Sikertelen forrás modosítása!");
        basedataSourcesPage.toastMessageButton.click();
        Thread.sleep(1000);
        basedataSourcesPage.revertEditSource();
        basedataSourcesPage.toastMessageButton.click();
    }
    @Test(priority = 5)
    public void testArchiveSource() throws InterruptedException {
        basedataSourcesPage.archiveSource();
        basedataSourcesPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres forrás archiválás!"), "Sikertelen forrás archiválás!");
        basedataSourcesPage.toastMessageButton.click();
        Thread.sleep(1000);
        basedataSourcesPage.revertArchiveSource();
        basedataSourcesPage.toastMessageButton.click();
    }
    @Test(priority = 6)
    public void testCreateSource() throws InterruptedException {
        basedataSourcesPage.createSource();
        basedataSourcesPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres forrás modosítása!"), "Sikertelen forrás modosítása!");
        basedataSourcesPage.toastMessageButton.click();
    }
    @Test(priority = 7)
    public void testDeleteSource(){
        basedataSourcesPage.deleteSource();
        basedataSourcesPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres forrás törlés!"), "Sikertelen forrás törlés!");
        basedataSourcesPage.toastMessageButton.click();
    }
}
