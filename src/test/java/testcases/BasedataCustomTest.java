package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.BasedataCustomPage;

public class BasedataCustomTest extends BaseTest {
    private BasedataCustomPage basedataCustomPage;
    @BeforeClass
    public void setUpBaseDataCustomFieldPage() {
        loginToWebsite();
        basedataCustomPage = new BasedataCustomPage(driver);
        BasedataCustomPage.basedataButton.click();
        BasedataCustomPage.dynamicFieldsButton.click();
    }
    @Test(priority = 1)
    public void testSearchCustom() throws InterruptedException {
        basedataCustomPage.searchCustomField();
        basedataCustomPage.waitForElementToBeVisible(basedataCustomPage.sourceOther);
        boolean result = basedataCustomPage.sourceOther.isDisplayed();
        Assert.assertTrue(result, "Sikertelen keresés!");
        driver.navigate().refresh();
    }
    @Test(priority = 2)
    public void testSortCustomField() throws InterruptedException {
        basedataCustomPage.sortCustomField();
        Assert.assertTrue(basedataCustomPage.columnSortAscend.isDisplayed(), "Sikertelen rendezés!");
    }
    @Test(priority = 3)
    public void testColumnMove() throws InterruptedException {
        basedataCustomPage.columnMove(basedataCustomPage.columnSort);
        Assert.assertTrue(basedataCustomPage.columnHeader.isDisplayed(), "Sikertelen oszlop mozgatás!");
        driver.navigate().refresh();
    }
    //Sikeres forrás archiválás!
    @Test(priority = 4)
    public void testEditCustomField() throws InterruptedException {
        basedataCustomPage.editCustomField();
        basedataCustomPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres dinamikus adat szerkesztés!"), "Sikertelen dinamikus adat szerkesztés!");
        basedataCustomPage.toastMessageButton.click();
        Thread.sleep(1000);
        basedataCustomPage.revertEditCustomField();
        basedataCustomPage.toastMessageButton.click();
    }
    @Test(priority = 5)
    public void testCreateCustomField() throws InterruptedException {
        basedataCustomPage.createCustomField();
        basedataCustomPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres dinamikus adat létrehozás!"), "Sikertelen dinamikus adat létrehozás!");
        basedataCustomPage.toastMessageButton.click();
    }
    @Test(priority = 6)
    public void testDeleteCustomField(){
        basedataCustomPage.deleteCustomField();
        basedataCustomPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres dinamikus adat törlés!"), "Sikertelen dinamikus adat törlés!");
        basedataCustomPage.toastMessageButton.click();
    }
}
