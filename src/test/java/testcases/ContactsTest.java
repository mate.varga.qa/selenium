package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ContactsPage;

public class ContactsTest extends BaseTest {
    private ContactsPage contactsPage;

    @BeforeClass
    public void setUpContactsPage() {
        loginToWebsite();
        contactsPage = new ContactsPage(driver);
        ContactsPage.contactsButton.click();
    }

    @Test(priority = 4)
    public void testContactsFilter() throws InterruptedException {
        contactsPage.contactsFilter();
        boolean result = contactsPage.contactsFilterCheck(contactsPage.keyword);
        Assert.assertTrue(result, "Sikertelen szűrés!");
    }

    @Test(priority = 5)
    public void testContactsFilterDelete() {
        contactsPage.contactsFilterDelete();
        boolean isNotChecked = !contactsPage.filterCheckbox.isSelected();
        Assert.assertTrue(isNotChecked, "Sikertelen szűrés törlés!");
    }
    @Test(priority = 6)
    public void testContactsFilterSave() throws InterruptedException {
        contactsPage.contactsFilterSave();
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres szűrő létrehozás!"), "Sikertelen szűrő létrehozás!");
        contactsPage.myFilters.click();
        contactsPage.contactsFilterDelete();
        contactsPage.toastMessageButton.click();
    }
    @Test(priority = 1)
    public void testContactsABCFilter() throws InterruptedException {
        contactsPage.contactsFilterByCharacter();
        boolean select = contactsPage.abcFilter_A.isEnabled();
        Assert.assertTrue(select, "Sikertelen szűrés karakter szerint!");
        contactsPage.abcFilter_All.click();
    }
    @Test(priority = 2)
    public void testColumnSort() throws InterruptedException {
        contactsPage.columnSort();
        Assert.assertTrue(contactsPage.columnSortAscend.isDisplayed(), "Sikertelen oszlop rendezés!");
    }
    @Test(priority = 3)
    public void testColumnMove() throws InterruptedException {
        contactsPage.columnMove(contactsPage.columnSort);
        Assert.assertTrue(contactsPage.columnHeader.isDisplayed(), "Sikertelen oszlop mozgatás!");
        driver.navigate().refresh();
    }
    @Test(priority = 7)
    public void testCreateCandidate() throws InterruptedException {
        contactsPage.createContact();
        Thread.sleep(2000);
        Assert.assertTrue(contactsPage.createButton.isDisplayed(), "Sikertelen vevő létrehozás!");
    }
    @Test(priority = 8)
    public void testChangeView(){
        contactsPage.changeView();
        Assert.assertTrue(contactsPage.changeViewButton.isDisplayed(), "Sikertelen nézet váltás!");
    }
    @Test(priority = 9)
    public void testAddColumns() throws InterruptedException {
        contactsPage.columnAdder();
        Thread.sleep(1000);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres nézet mentés!"), "Sikertelen nézet mentés!");
        contactsPage.toastMessageButton.click();
    }
}
