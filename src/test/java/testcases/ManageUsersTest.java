package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ManageUsersPage;


public class ManageUsersTest extends BaseTest{
    private ManageUsersPage manageUsersPage;

    @BeforeClass
    public void setUpManageUsersPage() {
        loginToWebsite();
        BasePage.userButton.click();
        manageUsersPage = new ManageUsersPage(driver);
        manageUsersPage.manageUsersButton.click();
    }
    @Test(priority = 1)
    public void testCreateUsers() throws InterruptedException {
        manageUsersPage.createUser();
        manageUsersPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres felhasználó létrehozás!"), "Sikertelen felhasználó létrehozás!");
        manageUsersPage.toastMessageButton.click();
    }
    @Test(priority = 3)
    public void testDeleteUser() {
        manageUsersPage.deleteUser();
        manageUsersPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres felhasználó törlés!"), "Sikertelen felhasználó törlés!");
    }
    @Test(priority = 2)
    public void testEditUser() throws InterruptedException {
        manageUsersPage.editUser();
        manageUsersPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres felhasználó módosítás!"), "Sikertelen felhasználó módosítás!");
        manageUsersPage.toastMessageButton.click();
    }
}
