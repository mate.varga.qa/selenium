package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.PositionsDetailsAdsPage;

public class PositionsDetailsAdsTest extends BaseTest {
    private PositionsDetailsAdsPage positionsDetailsAdsPage;

    @BeforeClass
    public void setUpPartnersDetailsPage() {
        loginToWebsite();
        positionsDetailsAdsPage = new PositionsDetailsAdsPage(driver);
        PositionsDetailsAdsPage.positionsButton.click();
        positionsDetailsAdsPage.firstPosition.click();
        positionsDetailsAdsPage.positionsAdsButton.click();
    }
    @Test(priority = 1)
    public void testCreateAd() throws InterruptedException {
        positionsDetailsAdsPage.createAd();
        positionsDetailsAdsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres hirdetési felület létrehozás!"), "Sikertelen hirdetési felület létrehozás!");
        positionsDetailsAdsPage.toastMessageButton.click();
    }
    @Test(priority = 3)
    public void testDeleteAd() {
        positionsDetailsAdsPage.deleteAd();
        positionsDetailsAdsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres hirdetés törlés!"), "Sikertelen hirdetés törlés!");
        positionsDetailsAdsPage.toastMessageButton.click();
    }
    @Test(priority = 2)
    public void testEditAd() throws InterruptedException {
        positionsDetailsAdsPage.editAd();
        positionsDetailsAdsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres hirdetési felület módosítás!"), "Sikertelen hirdetési felület módosítás!");
        positionsDetailsAdsPage.toastMessageButton.click();
    }
}
