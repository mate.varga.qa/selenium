package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.PartnersDetailsPage;

public class PartnersDetailsTest extends BaseTest {
    private PartnersDetailsPage partnersDetailsPage;

    @BeforeClass
    public void setUpPartnersDetailsPage() {
        loginToWebsite();
        partnersDetailsPage = new PartnersDetailsPage(driver);
        partnersDetailsPage.firstPartner.click();
    }
    @Test(priority = 4)
    public void testSwitch() {
        partnersDetailsPage.switchToggle();
        partnersDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres státusz módosítás!"), "Sikertelen státusz módosítás!");
        partnersDetailsPage.toastMessageButton.click();
    }
    @Test(priority = 1)
    public void testWriteHistoryMessage() {
        partnersDetailsPage.writeHistoryMessage("Teszt jegyzet");
        partnersDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres üzenet létrehozás!"), "Sikertelen üzenet létrehozás!");
        partnersDetailsPage.toastMessageButton.click();
    }
    @Test(priority = 2)
    public void testShowFullHistory() throws InterruptedException {
        partnersDetailsPage.showFullHistory();
        Assert.assertTrue(partnersDetailsPage.showFullHistory.isDisplayed(), "Sikertelen teljes történet megjelenítés!");
    }
    @Test(priority = 3)
    public void testCreateProject() throws InterruptedException {
        partnersDetailsPage.createProject();
        partnersDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres projekt létrehozás!"), "Sikertelen projekt létrehozás!");
        partnersDetailsPage.toastMessageButton.click();
    }
    @Test(priority = 6)
    public void testDeleteProject() {
        partnersDetailsPage.deleteProject();
        partnersDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Projekt sikeresen eltávolítva"), "Sikertelen projekt eltávolítás!");
    }
    @Test(priority = 5)
    public void testEditPartner() {
        partnersDetailsPage.editPartner();
        partnersDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres vevő módosítás!"), "Sikertelen vevő módosítás!");
        partnersDetailsPage.toastMessageButton.click();
        partnersDetailsPage.revertEditPartner();
        partnersDetailsPage.toastMessageButton.click();
    }
}
