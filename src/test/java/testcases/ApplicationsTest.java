package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ApplicationsPage;

public class ApplicationsTest extends BaseTest {
    private ApplicationsPage applicationsPage;

    @BeforeClass
    public void setUpApplicationsPage() {
        loginToWebsite();
        applicationsPage = new ApplicationsPage(driver);
        ApplicationsPage.applicationsButton.click();
    }

    @Test(priority = 4)
    public void testApplicationsFilter() throws InterruptedException {
        applicationsPage.applicationsFilter();
        boolean result = applicationsPage.applicationsFilterCheck(applicationsPage.keyword);
        Assert.assertTrue(result, "Sikertelen szűrés!");
    }

    @Test(priority = 5)
    public void testApplicationsFilterDelete() {
        applicationsPage.applicationsFilterDelete();
        boolean isNotChecked = !applicationsPage.filterCheckbox.isSelected();
        Assert.assertTrue(isNotChecked, "Sikertelen szűrés törlés!");
    }
    @Test(priority = 6)
    public void testApplicationsFilterSave() throws InterruptedException {
        applicationsPage.applicationsFilterSave();
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres szűrő létrehozás!"), "Sikertelen szűrő létrehozás!");
        applicationsPage.myFilters.click();
        applicationsPage.applicationsFilterDelete();
        applicationsPage.toastMessageButton.click();
    }
    @Test(priority = 1)
    public void testApplicationsABCFilter() throws InterruptedException {
        applicationsPage.applicationsFilterByCharacter();
        boolean select = applicationsPage.abcFilter_A.isEnabled();
        Assert.assertTrue(select, "Sikertelen szűrés karakter szerint!");
        applicationsPage.abcFilter_All.click();
    }
    @Test(priority = 2)
    public void testColumnSort() throws InterruptedException {
        applicationsPage.columnSort();
        Assert.assertTrue(applicationsPage.columnSortAscend.isDisplayed(), "Sikertelen oszlop rendezés!");
    }
    @Test(priority = 3)
    public void testColumnMove() throws InterruptedException {
        applicationsPage.columnMove(applicationsPage.columnSort);
        Assert.assertTrue(applicationsPage.columnHeader.isDisplayed(), "Sikertelen oszlop mozgatás!");
        driver.navigate().refresh();
    }
    @Test(priority = 7)
    public void testChangeView(){
        applicationsPage.changeView();
        Assert.assertTrue(applicationsPage.changeViewButton.isDisplayed(), "Sikertelen nézet váltás!");
    }
    @Test(priority = 8)
    public void testAddColumns() throws InterruptedException {
        applicationsPage.columnAdder();
        Thread.sleep(1000);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres nézet mentés!"), "Sikertelen nézet mentés!");
        applicationsPage.toastMessageButton.click();
    }
}
