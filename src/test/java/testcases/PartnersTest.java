package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.PartnersPage;

public class PartnersTest extends BaseTest {
    private PartnersPage partnersPage;

    @BeforeClass
    public void setUpPartnersPage() {
        loginToWebsite();
        partnersPage = new PartnersPage(driver);
    }

    @Test(priority = 4)
    public void testPartnersFilter() throws InterruptedException {
        partnersPage.partnersFilter();
        boolean result = partnersPage.partnersFilterCheck(partnersPage.keyword);
        Assert.assertTrue(result, "Sikertelen szűrés!");
    }

    @Test(priority = 5)
    public void testPartnersFilterDelete() {
        partnersPage.partnersFilterDelete();
        boolean isNotChecked = !partnersPage.filterCheckbox.isSelected();
        Assert.assertTrue(isNotChecked, "Sikertelen szűrés törlés!");
    }
    @Test(priority = 6)
    public void testPartnersFilterSave() throws InterruptedException {
        partnersPage.partnersFilterSave();
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres szűrő létrehozás!"), "Sikertelen szűrő létrehozás!");
        partnersPage.myFilters.click();
        partnersPage.partnersFilterDelete();
    }
    @Test(priority = 1)
    public void testPartnersABCFilter() throws InterruptedException {
        partnersPage.partnersFilterByCharacter();
        boolean select = partnersPage.abcFilter_A.isEnabled();
        Assert.assertTrue(select, "Sikertelen szűrés karakter szerint!");
        partnersPage.abcFilter_All.click();
    }
    @Test(priority = 2)
    public void testColumnSort() throws InterruptedException {
        partnersPage.columnSort();
        Assert.assertTrue(partnersPage.columnSortAscend.isDisplayed(), "Sikertelen oszlop rendezés!");
    }
    @Test(priority = 3)
    public void testColumnMove() throws InterruptedException {
        partnersPage.columnMove(partnersPage.columnSort);
        Assert.assertTrue(partnersPage.columnHeader.isDisplayed(), "Sikertelen oszlop mozgatás!");
        driver.navigate().refresh();
    }
    @Test(priority = 7)
    public void testbulkEdit() throws InterruptedException {
        partnersPage.bulkEdit();
        boolean isNotChecked = !partnersPage.bulkEditCheckbox_1.isSelected() && !partnersPage.bulkEditCheckbox_2.isSelected();
        Assert.assertTrue(isNotChecked, "Sikertelen csoportos szerkesztés!");
    }
    @Test(priority = 8)
    public void testCreatePartner() throws InterruptedException {
        partnersPage.createPartner();
        partnersPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres vevő létrehozás!"), "Sikertelen vevő létrehozás!");
    }
}
