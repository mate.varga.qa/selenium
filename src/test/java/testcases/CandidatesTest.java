package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.CandidatesPage;

public class CandidatesTest extends BaseTest {
    private CandidatesPage candidatesPage;

    @BeforeClass
    public void setUpCandidatesPage() {
        loginToWebsite();
        candidatesPage = new CandidatesPage(driver);
        CandidatesPage.candidatesButton.click();
    }

    @Test(priority = 4)
    public void testCandidatesFilter() throws InterruptedException {
        candidatesPage.candidatesFilter();
        boolean result = candidatesPage.candidatesFilterCheck(candidatesPage.keyword);
        Assert.assertTrue(result, "Sikertelen szűrés!");
    }

    @Test(priority = 5)
    public void testCandidatesFilterDelete() {
        candidatesPage.candidatesFilterDelete();
        boolean isNotChecked = !candidatesPage.filterCheckbox.isSelected();
        Assert.assertTrue(isNotChecked, "Sikertelen szűrés törlés!");
    }
    @Test(priority = 6)
    public void testCandidatesFilterSave() throws InterruptedException {
        candidatesPage.candidatesFilterSave();
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres szűrő létrehozás!"), "Sikertelen szűrő létrehozás!");
        candidatesPage.myFilters.click();
        candidatesPage.candidatesFilterDelete();
    }
    @Test(priority = 1)
    public void testCandidatesABCFilter() throws InterruptedException {
        candidatesPage.candidatesFilterByCharacter();
        boolean select = candidatesPage.abcFilter_A.isEnabled();
        Assert.assertTrue(select, "Sikertelen szűrés karakter szerint!");
        candidatesPage.abcFilter_All.click();
    }
    @Test(priority = 2)
    public void testColumnSort() throws InterruptedException {
        candidatesPage.columnSort();
        Assert.assertTrue(candidatesPage.columnSortAscend.isDisplayed(), "Sikertelen oszlop rendezés!");
    }
    @Test(priority = 3)
    public void testColumnMove() throws InterruptedException {
        candidatesPage.columnMove(candidatesPage.columnSort);
        Assert.assertTrue(candidatesPage.columnHeader.isDisplayed(), "Sikertelen oszlop mozgatás!");
        driver.navigate().refresh();
    }
    @Test(priority = 7)
    public void testbulkEdit() throws InterruptedException {
        candidatesPage.bulkEdit();
        boolean isNotChecked = !candidatesPage.bulkEditCheckbox_1.isSelected() && !candidatesPage.bulkEditCheckbox_2.isSelected();
        Assert.assertTrue(isNotChecked, "Sikertelen csoportos szerkesztés!");
    }
    @Test(priority = 8)
    public void testCreateCandidate() throws InterruptedException {
        candidatesPage.createCandidate();
        Thread.sleep(1000);
        Assert.assertTrue(candidatesPage.createButton.isDisplayed(), "Sikertelen vevő létrehozás!");
    }
}
