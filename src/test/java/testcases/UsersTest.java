package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.UsersPage;

public class UsersTest extends BaseTest {
    private UsersPage usersPage;

    @BeforeClass
    public void setUpUsersPage() {
        loginToWebsite();
        usersPage = new UsersPage(driver);
        BasePage.userButton.click();
    }
    @Test(priority = 1)
    public void testSetNewPassword() {
        usersPage.setNewPassword(Config.getProperty("password"), Config.getProperty("new.password"));
        usersPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres jelszó módosítás!"), "Sikertelen jelszó módosítás!");
    }
    @Test(priority = 3)
    public void testLogout() {
        usersPage.logout();
        usersPage.waitForElementToBeVisible(UsersPage.loginTitle);
        Assert.assertTrue(UsersPage.loginTitle.getText().contains("Bejelentkezés"), "Sikertelen kijelentkezés!");
    }
    @Test(priority = 2)
    public void testRevertPassword() {
        usersPage.setNewPassword(Config.getProperty("new.password"), Config.getProperty("password"));
        usersPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres jelszó módosítás!"), "Sikertelen jelszó módosítás!");
    }
}
