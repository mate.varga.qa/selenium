package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.PositionsDetailsPage;

public class PositionsDetailsTest extends BaseTest {
    private PositionsDetailsPage positionsDetailsPage;

    @BeforeClass
    public void setUpPartnersDetailsPage() {
        loginToWebsite();
        positionsDetailsPage = new PositionsDetailsPage(driver);
        PositionsDetailsPage.positionsButton.click();
        positionsDetailsPage.firstPosition.click();
    }
    @Test(priority = 1)
    public void testWriteHistoryMessage() {
        positionsDetailsPage.writeHistoryMessage("Teszt jegyzet");
        positionsDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres üzenet létrehozás!"), "Sikertelen üzenet létrehozás!");
    }
    @Test(priority = 2)
    public void testShowFullHistory() throws InterruptedException {
        positionsDetailsPage.showFullHistory();
        Assert.assertTrue(BasePage.showFullHistory.isDisplayed(), "Sikertelen teljes történet megjelenítés!");
    }
    @Test(priority = 5)
    public void testEditPosition() {
        positionsDetailsPage.editPosition();
        positionsDetailsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres pozíció módosítás!"), "Sikertelen pozíció módosítás!");
        positionsDetailsPage.toastMessageButton.click();
        positionsDetailsPage.revertEditPosition();
    }
}
