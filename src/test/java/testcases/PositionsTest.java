package testcases;

import base.BasePage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.PositionsPage;

public class PositionsTest extends BaseTest {
    private PositionsPage positionsPage;

    @BeforeClass
    public void setUpPositionsPage() {
        loginToWebsite();
        positionsPage = new PositionsPage(driver);
        PositionsPage.positionsButton.click();
    }

    @Test(priority = 4)
    public void testPositionsFilter() throws InterruptedException {
        positionsPage.positionsFilter();
        boolean result = positionsPage.positionsFilterCheck(positionsPage.keyword);
        Assert.assertTrue(result, "Sikertelen szűrés!");
    }

    @Test(priority = 5)
    public void testPositionsFilterDelete() {
        positionsPage.positionsFilterDelete();
        boolean isNotChecked = !positionsPage.filterCheckbox.isSelected();
        Assert.assertTrue(isNotChecked, "Sikertelen szűrés törlés!");
    }
    @Test(priority = 6)
    public void testPositionsFilterSave() throws InterruptedException {
        positionsPage.positionsFilterSave();
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres szűrő létrehozás!"), "Sikertelen szűrő létrehozás!");
        positionsPage.myFilters.click();
        positionsPage.positionsFilterDelete();
    }
    @Test(priority = 1)
    public void testPositionsABCFilter() throws InterruptedException {
        positionsPage.positionsFilterByCharacter();
        boolean select = positionsPage.abcFilter_A.isEnabled();
        Assert.assertTrue(select, "Sikertelen szűrés karakter szerint!");
        positionsPage.abcFilter_All.click();
    }
    @Test(priority = 2)
    public void testColumnSort() throws InterruptedException {
        positionsPage.columnSort();
        Assert.assertTrue(positionsPage.columnSortAscend.isDisplayed(), "Sikertelen oszlop rendezés!");
    }
    @Test(priority = 3)
    public void testColumnMove() throws InterruptedException {
        positionsPage.columnMove(positionsPage.columnSort);
        Assert.assertTrue(positionsPage.columnHeader.isDisplayed(), "Sikertelen oszlop mozgatás!");
        driver.navigate().refresh();
    }
    @Test(priority = 7)
    public void testbulkEdit() throws InterruptedException {
        positionsPage.bulkEdit();
        boolean isNotChecked = !positionsPage.bulkEditCheckbox_1.isSelected() && !positionsPage.bulkEditCheckbox_2.isSelected();
        Assert.assertTrue(isNotChecked, "Sikertelen csoportos szerkesztés!");
    }
    @Test(priority = 8)
    public void testCreatePosition() throws InterruptedException {
        positionsPage.createPosition();
        positionsPage.waitForElementToBeVisible(BasePage.toastMessage);
        Assert.assertTrue(BasePage.toastMessage.getText().contains("Sikeres pozíció létrehozás!"), "Sikertelen pozíció létrehozás!");
    }
}
