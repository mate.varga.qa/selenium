package testcases;

import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LoginPage;

public class LoginTest extends BaseTest {
    private LoginPage loginPage;

    @BeforeClass
    public void setUp() {
        loginPage = new LoginPage(driver);
    }

    @Test
    public void testLoginSuccess(){
        loginPage.clearLoginForm();
        loginPage.login(Config.getProperty("email"), Config.getProperty("password"));
        loginPage.waitForTitle();
        if (loginPage.partnersTitle.getText().contains("Vevők")){
            Assert.assertTrue(loginPage.partnersTitle.isDisplayed(), "Sikertelen bejelentkezés");
        }
    }

    @Test
    public void testLoginFail(){
        loginPage.login("username@email.com", "Password123");
        Assert.assertTrue(loginPage.isToastMessageDisplayed(), "Nem jelenik meg a hibaüzenet");
        System.out.println(loginPage.toastMessage.getText());
        loginPage.closeMessage();
    }

    @Test (groups = {"NavigateBack"})
    public void testForgotPasswordSuccess(){
        loginPage.forgotPassword(Config.getProperty("email"));
        loginPage.waitForElementToBeVisible(loginPage.toastMessage);
        if (loginPage.toastMessage.getText().contains("Egy elfelejtett jelszó email már ki lett küldve. Amíg az nem jár le nem kérhető új!")){
            Assert.assertFalse(loginPage.toastMessage.isDisplayed(), "Túl gyakran kértél jelszó emlékeztetőt.");
        } else {
            Assert.assertEquals(loginPage.toastMessage.getText(), "Sikeres e-mail küldés!", "Nem sikerült elküldeni az e-mailt a jelszó visszaállításához.");
        }
        loginPage.closeMessage();
        driver.navigate().back();
    }

    @Test (groups = {"NavigateBack"})
    public void testForgotPasswordFail(){
        loginPage.forgotPassword("username@email.com");
        loginPage.waitForElementToBeVisible(loginPage.toastMessage);
        Assert.assertEquals(loginPage.toastMessage.getText(), "Sikertelen e-mail küldés!!", "Ismeretlen hiba lépett fel.");
        loginPage.closeMessage();
    }
}
