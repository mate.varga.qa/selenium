package base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.LoginPage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseTest{
    protected WebDriver driver;
    public static Logger log = LogManager.getLogger(BaseTest.class);
    public static Properties Config = new Properties();

    @BeforeSuite
    public void fileConfig() throws IOException {
        System.setProperty("log4j.configurationFile", System.getProperty("user.dir") + "\\src\\main\\resources\\log4j2.xml");
        log.info("Log4J2 properties file loaded");

        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\properties\\Config.properties");
        Config.load(fis);
        log.info("Config properties file loaded");
    }

    @BeforeClass
    public void baseSetUp() {
            if (Config.getProperty("browser").equals("chrome")) {
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                log.info("Google Chrome launched");
            } else if (Config.getProperty("browser").equals("firefox")) {
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                log.info("Mozilla Firefox launched");
            } else if (Config.getProperty("browser").equals("edge")) {
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                log.info("Microsoft Edge launched");
            }
            driver.manage().window().maximize();
            driver.get(Config.getProperty("url"));
            driver.manage().timeouts().implicitlyWait(Integer.parseInt(Config.getProperty("implicit.wait")), TimeUnit.SECONDS);
    }
    public void loginToWebsite(){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(Config.getProperty("email"), Config.getProperty("password"));
    }

    @AfterMethod(alwaysRun = true)
    public void checkForErrors(ITestResult result) throws InterruptedException {
        if (!result.isSuccess()) {
            Throwable t = result.getThrowable();
            if (t != null) {
                // Itt logolhatja a hibaüzenetet
                log.error("Error in test: " + result.getName());
                log.error("Error message: " + t.getMessage());
                // További logolás, ha szükséges
            }
        }
        List<String> groups = Arrays.asList(result.getMethod().getGroups());
        if(groups.contains("NavigateBack")) {
            driver.navigate().back();
        }
        Thread.sleep(1000);
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }


}
