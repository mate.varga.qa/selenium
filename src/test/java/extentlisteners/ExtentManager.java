package extentlisteners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentManager {
	private static final String reportFileName = ExtentListeners.fileName;
	public static ExtentReports createInstance(String fileName) {
		ExtentSparkReporter htmlReporter = new ExtentSparkReporter(fileName);

		htmlReporter.config().setTheme(Theme.STANDARD);
		htmlReporter.config().setDocumentTitle(fileName);
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setReportName(reportFileName);
		htmlReporter.config().setTimeStampFormat("yyyy. MMMM dd. HH:mm:ss '('zzz')'");

		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		extent.setSystemInfo("Username", System.getProperty("user.name"));
		extent.setSystemInfo("Operating system", System.getProperty("os.name"));
		extent.setSystemInfo("Java Version", System.getProperty("java.version"));
		extent.setSystemInfo("Java Class Version", System.getProperty("java.class.version"));

		return extent;
	}
}
