package extentlisteners;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import com.aventstack.extentreports.MediaEntityBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import static base.BasePage.driver;

public class ExtentListeners implements ITestListener {
	static Date date = new Date();
	static SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
	static String fileName = "Report_" + simpleDate.format(date) + ".html";
	private static final ExtentReports extent = ExtentManager.createInstance(System.getProperty("user.dir")+"\\reports\\"+fileName);
	private static final Logger log = (Logger) LogManager.getLogger(ExtentListeners.class);
	public static ExtentTest test;

	public void onStart(ITestContext context) {
		log.info("Test Suite " + context.getName() + " started");
	}

	public void onFinish(ITestContext context) {
        log.info("Test Suite " + context.getName() + " ending");
        extent.flush();
    }

	public void onTestStart(ITestResult result) {
		String fullClassName = result.getTestClass().getName();
		String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
		test = extent.createTest(className + ": " + result.getMethod().getMethodName());
		log.info("Running test method " + result.getMethod().getMethodName() + " of class " + className);
	}

	public void onTestSuccess(ITestResult result) {
		String methodName = result.getMethod().getMethodName();
		log.info("Test passed: " + methodName);
		String logText="<b>"+"TEST CASE:- "+ methodName.toUpperCase()+ " PASSED"+"</b>";		
		Markup m=MarkupHelper.createLabel(logText, ExtentColor.GREEN);
		test.pass(m);
	}

	public void onTestFailure(ITestResult result) {
		Date screenshotDate = new Date();
		SimpleDateFormat screenshotSimpleDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

		String excepionMessage = Arrays.toString(result.getThrowable().getStackTrace());
		test.fail(excepionMessage);

		String methodName = result.getMethod().getMethodName();
		log.error("Test failed: " + methodName, result.getThrowable());
		String logText = "<b>"+"TEST CASE:- "+ methodName.toUpperCase()+ " FAILED"+"</b>";

		String screenshotFileName = "Screenshot_" + screenshotSimpleDate.format(screenshotDate) + ".jpg";
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File(System.getProperty("user.dir") + "\\reports\\" + screenshotFileName));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		test.fail("<b><font color=red>" + "Screenshot of failure" + "</font></b><br>", MediaEntityBuilder.createScreenCaptureFromPath(screenshotFileName)
				.build());

		Markup m = MarkupHelper.createLabel(logText, ExtentColor.RED);
		test.log(Status.FAIL, m);
	}

	public void onTestSkipped(ITestResult result) {
		String methodName=result.getMethod().getMethodName();
		String logText="<b>"+"Test Case:- "+ methodName+ " Skipped"+"</b>";		
		Markup m=MarkupHelper.createLabel(logText, ExtentColor.YELLOW);
		test.skip(m);
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
	}
}
