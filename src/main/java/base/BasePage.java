package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {
    public static WebDriver driver;

    @FindBy(css = "div.toast-message")
    public static WebElement toastMessage;
    @FindBy(css = "body > hot-toast-container > div > div > hot-toast > div > div > div.hot-toast-message > div > app-toast-base > div > div.toast-close-icon > button")
    public WebElement toastMessageButton;
    @FindBy(xpath = "//img[@alt='Profile icon']")
    public static WebElement userButton;
    @FindBy(xpath = "//button[@class='filter-menu-button']")
    public WebElement filterButton;
    @FindBy(xpath = "//app-text-number-filtering[@class='ng-star-inserted']//app-checkbox")
    public WebElement filterCheckbox;
    @FindBy(xpath = "//app-filter-container-content[@class='ng-star-inserted']//p[contains(text(), 'Törlés')]")
    public WebElement filterClearButton;
    @FindBy(xpath = "//app-dot-menu[@class='white-dot-button']//button")
    public WebElement filterDotMenu;
    @FindBy(xpath = "//app-text-field[@formcontrolname='filterName']//input")
    public WebElement filterSaveNameField;
    @FindBy(xpath = "//app-select[@formcontrolname='filterVisibility']//div[@class='ng-input']")
    public WebElement filterSaveVisibilitySelect;
    @FindBy(xpath = "//div[contains(text(), 'Privát')]")
    public WebElement filterSaveVisibilityPrivate;
    @FindBy(xpath = "//ng-select[contains(@class, 'filter-ng-select')]")
    public WebElement myFilters;
    @FindBy(xpath = "//span[normalize-space()='A']")
    public WebElement abcFilter_A;
    @FindBy(xpath = "//span[contains(text(), 'Összes')]")
    public WebElement abcFilter_All;
    @FindBy(xpath = "//span[@class='sort-btn datatable-icon-up sort-asc']")
    public WebElement columnSortAscend;
    @FindBy(xpath = "//datatable-header-cell")
    public WebElement columnHeader;
    @FindBy(xpath = "//datatable-row-wrapper[1]//span[@class='checkbox-custom']")
    public WebElement bulkEditCheckbox_1;
    @FindBy(xpath = "//datatable-row-wrapper[2]//span[@class='checkbox-custom']")
    public WebElement bulkEditCheckbox_2;
    @FindBy(xpath = "//span[contains(text(), 'CSOPORTOS SZERKESZTÉS')]")
    public WebElement bulkEditButton;
    @FindBy(xpath = "//span[contains(text(),'Igen')]")
    public WebElement yesButton;
    @FindBy(xpath = "//button[normalize-space()='Mentés']")
    public WebElement saveButton;
    @FindBy(xpath = "//app-text-field[@label='partners.company_name']//input")
    public WebElement partnersCompanyName;
    @FindBy(xpath = "//app-text-field[@label='common.tax_number']//input")
    public WebElement partnersTaxNumber;
    @FindBy(xpath = "//app-select[@formcontrolname='officeId']")
    public WebElement partnersOffice;
    @FindBy(xpath = "//span[@class='ng-option-label ng-star-inserted']")
    public WebElement selectOption_0;
    @FindBy(xpath = "//span[contains(text(),'Telefonon nem elérhető')]")
    public WebElement selectOption_1;
    @FindBy(xpath = "//app-select[@label='common.country']//ng-select")
    public WebElement country;
    @FindBy(xpath = "//app-select[@label='common.settlement']//ng-select//input")
    public WebElement settlement;
    @FindBy(xpath = "//app-text-field[@label='common.street_house']//input")
    public WebElement streetHouse;
    @FindBy(xpath = "//app-yes-no-select[@label='candidates.newsletter_subscribed']//ng-select")
    public WebElement newsletter;
    @FindBy(xpath = "//app-select[@formcontrolname='preferredLanguageId']//ng-select")
    public WebElement preferredLanguage;
    @FindBy(xpath = "//app-yes-no-select[@label='candidates.need_send_terms_notification']//ng-select")
    public WebElement termsNotification;
    @FindBy(xpath = "//app-select[@formcontrolname='id']//ng-select")
    public WebElement partnersContact;
    @FindBy(xpath = "//app-primary-button[@class='action-button']/button/span[text()='Mentés']")
    public WebElement submitSaveButton;
    @FindBy(xpath = "//span[normalize-space()='Igen, törlöm']")
    public WebElement alternateConfirmButton;
    @FindBy(xpath = "//app-text-field[@formcontrolname='name']//input")
    public WebElement projectName;
    @FindBy(xpath = "//a[@ng-reflect-router-link='positions']//*[name()='svg']")
    public static WebElement positionsButton;
    @FindBy(xpath = "//datatable-row-wrapper[4]//datatable-body-cell[.//span[contains(text(), 'Almaszedő')]]")
    public WebElement firstPosition;
    @FindBy(xpath = "//button[@class='d-flex justify-content-center align-items-center p-0 m-0 primary-button']//*[name()='svg']")
    public static WebElement createCrossButton;
    @FindBy(xpath = "//button[@role='switch']")
    public WebElement switchButton;
    @FindBy(xpath = "//div[@role='combobox']")
    public static WebElement kanbanSortButton;
    @FindBy(xpath = "//app-primary-button[@class='ng-star-inserted']//span[@class='primary-button-text-value']")
    public WebElement createButton;
    @FindBy(xpath = "//app-text-field[@formcontrolname='lastName']//input")
    public WebElement lastName;
    @FindBy(xpath = "//app-text-field[@formcontrolname='firstName']//input")
    public WebElement firstName;
    @FindBy(xpath = "//app-text-field[@formcontrolname='email']//input")
    public WebElement email;
    @FindBy(xpath = "//app-text-field[@formcontrolname='phone']//input")
    public WebElement phoneNumber;
    @FindBy(xpath = "//app-datepicker[@label='common.birth_date']//button")
    public WebElement birthday;
    @FindBy(xpath = "//button[@aria-label='Choose month and year']")
    public WebElement monthYearButton;
    @FindBy(xpath = "//div[normalize-space()='2001']")
    public WebElement year2001;
    @FindBy(xpath = "//div[normalize-space()='JAN.']")
    public WebElement monthJanuary;
    @FindBy(xpath = "//table[@class='mat-calendar-table']//button[.//div[contains(text(),'10')]]")
    public WebElement dateNumber10;
    @FindBy(xpath = "//table[@class='mat-calendar-table']//button[.//div[contains(text(),'25')]]")
    public WebElement dateNumber25;
    @FindBy(xpath = "//app-select[@label='candidates.source']//ng-select")
    public WebElement source;
    @FindBy(xpath = "//app-select[@label='positions.partners']//ng-select")
    public WebElement positionsPartner;
    @FindBy(xpath = "//app-select[@class='ng-touched ng-pristine ng-invalid']//ng-select[.//div[@class='ng-input']]")
    public WebElement positionsProject;
    @FindBy(xpath = "//textarea[@placeholder='Jegyzet írása...']")
    public WebElement historyMessage;
    @FindBy(xpath = "//button[normalize-space()='Teljes történet mutatása']")
    public static WebElement showFullHistory;
    @FindBy(xpath = "//button[normalize-space()='Megjegyzések mutatása']")
    public WebElement hideFullHistory;
    @FindBy(xpath = "//a[@ng-reflect-router-link='candidates']//*[name()='svg']")
    public static WebElement candidatesButton;
    @FindBy(xpath = "//app-key-value-wrapper[@class='ng-star-inserted']//button[@class='key-value-header-icon']")
    public WebElement editPartnerButton;
    @FindBy(xpath = "//app-select[@label='candidates.partner_name']//ng-select")
    public WebElement partnersName;
    @FindBy(xpath = "//app-select[@ng-reflect-label='candidates.project_name']//ng-select")
    public WebElement positionsProject2;
    @FindBy(xpath = "//a[@ng-reflect-router-link='base-data']//*[name()='svg']")
    public static WebElement basedataButton;
    @FindBy(xpath = "//button[normalize-space()='Szerkesztés']")
    public WebElement dotMenuEditButton;
    @FindBy(xpath = "//button[normalize-space()='Törlés']")
    public WebElement deleteButton;

    public BasePage(WebDriver driver){
        BasePage.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void waitForElementToBeVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOf(element));
    }
    public boolean isToastMessageDisplayed() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOf(toastMessage));
        String errorText = toastMessage.getText();

        return errorText.contains(toastMessage.getText());
    }
    public void columnMove(WebElement element) throws InterruptedException {
        Actions action = new Actions(driver);
        int xOffset = 500;
        int yOffset = 0;
        action.clickAndHold(element)
                .pause(1000)
                .moveByOffset(xOffset, yOffset)
                .pause(1000)
                .release()
                .perform();
        Thread.sleep(1000);
    }

}
