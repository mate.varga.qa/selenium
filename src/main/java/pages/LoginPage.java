package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    public static String expectedUrl = "http://fe.szakdoga.com:4200/partners";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "app-text-field#email input")
    public  WebElement email;
    @FindBy(css = "app-text-field#password input")
    public  WebElement pass;
    @FindBy(css = "app-primary-button")
    public  WebElement submitButton;
    @FindBy(css = "span.forgot-password-link")
    public  WebElement forgotPasswordButton;
    @FindBy(css = "div.toast-message")
    public WebElement toastMessage;

    @FindBy(css = "app-root > div > section:nth-of-type(2) > app-header > app-main-page-header > div > div:nth-of-type(1) > h1\n")
    public WebElement partnersTitle;

    public void setEmail(String emailAddress) {
        email.sendKeys(emailAddress);
    }
    public void setPassword(String password) {
        pass.sendKeys(password);
    }
    public void clickSubmitButton() {
        submitButton.click();
    }
    public void clickForgotPasswordButton() {
        forgotPasswordButton.click();
    }
    public void clickToastMessageButton() {
        toastMessageButton.click();
    }
    public void clearLoginForm() {
        email.clear();
        pass.clear();
    }
    public void waitForTitle() {
        waitForElementToBeVisible(partnersTitle);
    }
    public PartnersPage login(String emailAddress, String password){
        setEmail(emailAddress);
        setPassword(password);
        clickSubmitButton();

        return new  PartnersPage(driver);
    }
    public void forgotPassword(String emailAddress) {
        clickForgotPasswordButton();
        email.sendKeys(emailAddress);
        submitButton.click();
    }
    public void closeMessage() {
        if (toastMessage.isDisplayed()){
            clickToastMessageButton();
        }

    }
}
