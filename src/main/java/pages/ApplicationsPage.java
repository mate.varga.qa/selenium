package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class ApplicationsPage extends BasePage {
    public ApplicationsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//a[@ng-reflect-router-link='position-applications']//*[name()='svg']")
    public static WebElement applicationsButton;
    @FindBy(xpath = "//input[@placeholder='Név']")
    public WebElement filterContactName;
    @FindBy(xpath = "//span[contains(text(),'Pozíció neve')]")
    public WebElement columnSort;
    @FindBy(xpath = "//app-select[@label='candidates.type']//ng-select")
    public WebElement candidatesType;
    @FindBy(xpath = "//app-select[@label='filters.explorer_user']//ng-select")
    public WebElement explorerUser;
    @FindBy(xpath = "//app-datepicker[@formcontrolname='exploredDate']//button")
    public WebElement exploredDate;
    @FindBy(xpath = "//app-select[@ng-reflect-placeholder='table_view.saved_views']//ng-select")
    public WebElement changeViewButton;
    @FindBy(xpath = "//div[@ng-reflect-inline-s-v-g='assets/image/custom-column-sel']//*[name()='svg']")
    public WebElement columnPicker;
    @FindBy(xpath = "//app-checkbox[@ng-reflect-label='Születési dátum']//span[@class='checkbox-custom']")
    public WebElement checkboxOption_1;
    @FindBy(xpath = "//app-checkbox[@ng-reflect-label='Email']//span[@class='checkbox-custom']")
    public WebElement checkboxOption_2;
    @FindBy(xpath = "//app-checkbox[@ng-reflect-label='Telefonszám']//span[@class='checkbox-custom']")
    public WebElement checkboxOption_3;
    @FindBy(xpath = "//app-checkbox[@ng-reflect-label='Szállásigény']//span[@class='checkbox-custom']")
    public WebElement checkboxOption_4;
    @FindBy(xpath = "//button[@class='dot-menu-trigger ng-star-inserted']//*[name()='svg']")
    public WebElement columnAdderDotMenu;
    @FindBy(xpath = "//app-text-field[@ng-reflect-placeholder='table_view.search']//input")
    public WebElement columnAdderSearch;
    @FindBy(xpath = "//button[normalize-space()='Mentés másként']")
    public WebElement saveAsButton;
    @FindBy(xpath = "//span[normalize-space()='Alkalmaz']")
    public WebElement applyButton;
    @FindBy(xpath = "//app-select[@ng-reflect-label='table_view.default_view']//ng-select")
    public WebElement defaultField;
    @FindBy(css = "body > app-root > div > section.main-content > main > app-partner-list > div > div > app-table > div > ngx-datatable > div > datatable-body")
    public List<WebElement> records;

    public String keyword = "ta";
    public void applicationsFilter() throws InterruptedException {
        filterButton.click();
        filterCheckbox.click();
        filterContactName.sendKeys(keyword);
        Thread.sleep(1500);
    }
    public boolean applicationsFilterCheck(String keyword){
        for (WebElement element : records) {
            if (!element.getText().contains(keyword)) {
                return false;
            }
        }
        return true;
    }
    public void applicationsFilterDelete(){
        filterClearButton.click();
        filterButton.click();
    }

    public void applicationsFilterSave() throws InterruptedException {
        filterButton.click();
        filterCheckbox.click();
        filterContactName.sendKeys(keyword);
        filterDotMenu.click();
        saveButton.click();
        filterSaveNameField.sendKeys(keyword);
        filterSaveVisibilitySelect.click();
        filterSaveVisibilityPrivate.click();
        Thread.sleep(1000);
        saveButton.click();
        waitForElementToBeVisible(toastMessage);
        myFilters.click();

    }
    public void applicationsFilterByCharacter() throws InterruptedException {
        abcFilter_A.click();
        Thread.sleep(1500);
    }

    public void columnSort() throws InterruptedException {
        waitForElementToBeVisible(columnSort);
        columnSort.click();
        Thread.sleep(1500);
    }
    public void changeView(){
        changeViewButton.click();
        selectOption_0.click();
    }
    public void columnAdder() throws InterruptedException {
        columnPicker.click();
        waitForElementToBeVisible(checkboxOption_1);
        checkboxOption_1.click();
        checkboxOption_2.click();
        checkboxOption_3.click();
        applyButton.click();
        Thread.sleep(2000);
        columnPicker.click();
        columnAdderSearch.sendKeys("Száll");
        checkboxOption_4.click();
        applyButton.click();
        columnAdderDotMenu.click();
        saveAsButton.click();
        projectName.sendKeys("Test View");
        defaultField.click();
        selectOption_0.click();
        saveButton.click();
    }
}
