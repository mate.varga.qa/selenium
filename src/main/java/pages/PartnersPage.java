package pages;

import base.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;


public class PartnersPage extends BasePage {
    public PartnersPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//input[@placeholder='Vevő név']")
    public WebElement filterContactName;
    @FindBy(xpath = "//span[contains(text(),'Iroda')]")
    public WebElement columnSort;
    @FindBy(xpath = "//app-select[@formcontrolname='fieldManagerId']")
    public WebElement partnersFieldManager;
    @FindBy(xpath = "//app-bulk-edit-confirm-modal[@class='ng-star-inserted']//span[text()='Igen']")
    public WebElement bulkEditConfirmButton;
    @FindBy(xpath = "//button[.//span[text()='Befejezés']]")
    public WebElement bulkEditFinishButton;
    @FindBy(xpath = "//app-datepicker[@label='common.date']//button")
    public WebElement partnersContractDate;
    @FindBy(css = "body > app-root > div > section.main-content > main > app-partner-list > div > div > app-table > div > ngx-datatable > div > datatable-body")
    public List<WebElement> records;

    public String keyword = "Lead";
    public void partnersFilter() throws InterruptedException {
        filterButton.click();
        filterCheckbox.click();
        filterContactName.sendKeys(keyword);
        Thread.sleep(1500);
    }
    public boolean partnersFilterCheck(String keyword){
        for (WebElement element : records) {
            if (!element.getText().contains(keyword)) {
                return false;
            }
        }
        return true;
    }
    public void partnersFilterDelete(){
        filterClearButton.click();
        filterButton.click();
    }

    public void partnersFilterSave() throws InterruptedException {
        filterButton.click();
        filterCheckbox.click();
        filterContactName.sendKeys(keyword);
        filterDotMenu.click();
        saveButton.click();
        filterSaveNameField.sendKeys(keyword);
        filterSaveVisibilitySelect.click();
        filterSaveVisibilityPrivate.click();
        Thread.sleep(1000);
        saveButton.click();
        waitForElementToBeVisible(toastMessage);
        myFilters.click();

    }
    public void partnersFilterByCharacter() throws InterruptedException {
        abcFilter_A.click();
        Thread.sleep(1500);
    }

    public void columnSort() throws InterruptedException {
        waitForElementToBeVisible(columnSort);
        columnSort.click();
        Thread.sleep(1500);
    }
    public void bulkEdit() throws InterruptedException {
        bulkEditCheckbox_1.click();
        bulkEditCheckbox_2.click();
        bulkEditButton.click();
        waitForElementToBeVisible(partnersOffice);
        partnersOffice.click();
        selectOption_0.click();
        partnersFieldManager.click();
        selectOption_0.click();
        Thread.sleep(1000);
        yesButton.click();
        bulkEditConfirmButton.click();
        bulkEditFinishButton.click();
    }
    public static class TaxNumberUtil {
        public String generateTaxNumber() {
            Random rand = new Random();
            StringBuilder sb = new StringBuilder();
            // Első 8 számjegy
            for (int i = 0; i < 8; i++) {
                sb.append(rand.nextInt(10));
            }
            sb.append('-');
            // 9. számjegy
            sb.append(rand.nextInt(10));
            sb.append('-');
            // Utolsó 2 számjegy
            for (int i = 0; i < 2; i++) {
                sb.append(rand.nextInt(10));
            }
            return sb.toString();
        }
    }
    public void createPartner() throws InterruptedException {
        createButton.click();
        waitForElementToBeVisible(partnersCompanyName);
        partnersCompanyName.sendKeys("Test Partner");
        String taxNumber = new TaxNumberUtil().generateTaxNumber();
        partnersTaxNumber.sendKeys(taxNumber);
        partnersOffice.click();
        selectOption_0.click();
        country.click();
        selectOption_0.click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", settlement);
        settlement.click();
        settlement.sendKeys("9022");
        selectOption_0.click();
        streetHouse.sendKeys("Test utca 1.");
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", partnersFieldManager);
        partnersFieldManager.click();
        selectOption_0.click();
        partnersContact.click();
        selectOption_0.click();
        partnersContractDate.click();
        waitForElementToBeVisible(dateNumber10);
        dateNumber10.click();
        Thread.sleep(1000);
        submitSaveButton.click();

    }

}
