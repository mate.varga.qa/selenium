package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UsersPage extends BasePage {
    public UsersPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//button[normalize-space()='+ Új jelszó beállítása']")
    public WebElement newPasswordButton;
    @FindBy(xpath = "//app-text-field[@formcontrolname='oldPassword']//input")
    public WebElement password_Old;
    @FindBy(xpath = "//app-text-field[@formcontrolname='password']//input")
    public WebElement password_New;
    @FindBy(xpath = "//app-text-field[@formcontrolname='password_confirmation']//input")
    public WebElement confirmPassword_New;
    @FindBy(xpath = "//button[normalize-space()='Kijelentkezés']")
    public WebElement logoutButton;
    @FindBy(xpath = "//h1[@class='auth-title']")
    public static WebElement loginTitle;

    public void setNewPassword(String oldPassword, String newPassword) {
        newPasswordButton.click();
        password_Old.sendKeys(oldPassword);
        password_New.sendKeys(newPassword);
        confirmPassword_New.sendKeys(newPassword);
        saveButton.click();
    }
    public void logout() {
        logoutButton.click();
    }
}
