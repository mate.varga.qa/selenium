package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CandidatesDetailsPage extends BasePage {
    public CandidatesDetailsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//datatable-row-wrapper[2]//datatable-body-cell[.//span[contains(text(), '/Test Candidate')]]")
    public WebElement firstCandidate;
    @FindBy(xpath = "//app-key-value-wrapper[@paneltitle='candidates.personal_details']//button[@type='button']")
    public WebElement editCandidateButton;
    @FindBy(xpath = "//app-select[@label='candidates.position_name']//ng-select")
    public WebElement positionsName;
    @FindBy(xpath = "//tbody/tr[1]/td[7]/div[1]/app-dot-menu[1]/button[1]")
    public WebElement positionDotMenu;
    @FindBy(xpath = "//button[normalize-space()='Pozíció leválasztása']")
    public WebElement detachCandidateButton;
    @FindBy(xpath = "//span[normalize-space()='Igen, leválasztom']")
    public WebElement detachConfirmButton;
    @FindBy(xpath = "//span[@class='status-name truncate']")
    public WebElement statusButton;

    @FindBy(xpath = "//*[name()='path' and contains(@d,'M6.19336 4')]")
    public WebElement deleteCandidateButton;

    public void writeHistoryMessage(String message) {
        historyMessage.sendKeys(message);
        saveButton.click();
    }
    public void showFullHistory() throws InterruptedException {
        showFullHistory.click();
        Thread.sleep(1500);
        hideFullHistory.click();
    }
    public void editCandidate() {
        editCandidateButton.click();
        waitForElementToBeVisible(lastName);
        lastName.clear();
        lastName.sendKeys("Test");
        firstName.clear();
        firstName.sendKeys("Candidate Edit");
        submitSaveButton.click();
    }
    public void revertEditCandidate() {
        editCandidateButton.click();
        waitForElementToBeVisible(lastName);
        lastName.clear();
        lastName.sendKeys("!Teszt");
        firstName.clear();
        firstName.sendKeys("Jelölt");
        submitSaveButton.click();
    }
    public void addCandidateToPosition() throws InterruptedException {
        createCrossButton.click();
        waitForElementToBeVisible(partnersName);
        partnersName.click();
        selectOption_0.click();
        positionsProject2.click();
        selectOption_0.click();
        positionsName.click();
        selectOption_0.click();
        Thread.sleep(1000);
        yesButton.click();
    }
    public void detachCandidateFromPosition() throws InterruptedException {
        positionDotMenu.click();
        detachCandidateButton.click();
        Thread.sleep(1000);
        detachConfirmButton.click();
    }
    public void changeStatus() {
        statusButton.click();
        selectOption_1.click();
    }
    public void deleteCandidate() {
        deleteCandidateButton.click();
        yesButton.click();
    }
}
