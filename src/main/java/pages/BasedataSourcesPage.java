package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BasedataSourcesPage extends BasePage{
    public BasedataSourcesPage(WebDriver driver) {
            super(driver);
    }
    @FindBy(xpath = "//app-text-field[@ng-reflect-placeholder='base_data.sources.search']//input")
    public WebElement sourceSearch;
    @FindBy(xpath = "//span[contains(text(),'Típus')]")
    public WebElement columnSort;
    @FindBy(xpath = "//datatable-body-cell[@role='cell']//app-dot-menu[@class='justify-content-center d-flex']//button")
    public WebElement sourceDotMenu;
    @FindBy(xpath = "//button[normalize-space()='Archiválás']")
    public WebElement sourceArchive;
    @FindBy(xpath = "//button[normalize-space()='Aktiválás']")
    public WebElement sourceActivate;
    @FindBy(xpath = "//button[normalize-space()='Forrás végleges törlése']")
    public WebElement sourcePermanentDelete;
    @FindBy(xpath = "//button[@class='filled warn']")
    public WebElement yesButton2;
    @FindBy(xpath = "//span[@title='Facebook']")
    public WebElement sourceFacebook;
    @FindBy(xpath = "//app-select[@formcontrolname='type']//ng-select")
    public WebElement sourceType;

    public void searchSource() throws InterruptedException {
        waitForElementToBeVisible(sourceSearch);
        sourceSearch.sendKeys("face");
        Thread.sleep(1000);
    }
    public void sortSource() throws InterruptedException {
        columnSort.click();
        Thread.sleep(1000);
        columnSort.click();
        Thread.sleep(1000);
        columnSort.click();
    }
    public void editSource() {
        sourceDotMenu.click();
        dotMenuEditButton.click();
        projectName.sendKeys(" - Test");
        yesButton.click();
    }
    public void revertEditSource() {
        sourceDotMenu.click();
        dotMenuEditButton.click();
        projectName.clear();
        projectName.sendKeys("Adatbázis");
        yesButton.click();
    }
    public void archiveSource() {
        sourceDotMenu.click();
        sourceArchive.click();
        yesButton2.click();
    }
    public void revertArchiveSource() {
        sourceDotMenu.click();
        sourceActivate.click();
        yesButton.click();
    }
    public void createSource() throws InterruptedException {
        createCrossButton.click();
        projectName.sendKeys("!Test Source");
        sourceType.click();
        selectOption_0.click();
        Thread.sleep(1000);
        yesButton.click();
    }
    public void deleteSource() {
        sourceDotMenu.click();
        waitForElementToBeVisible(sourcePermanentDelete);
        sourcePermanentDelete.click();
        alternateConfirmButton.click();
    }
}
