package pages;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class CandidatesPage extends BasePage {
    public CandidatesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@placeholder='Név']")
    public WebElement filterContactName;
    @FindBy(xpath = "//span[contains(text(),'Lakóhely')]")
    public WebElement columnSort;
    @FindBy(xpath = "//app-datepicker[@label='candidates.membership_end']//button")
    public WebElement candidatesMembershipDate;
    @FindBy(xpath = "//app-select[@ng-reflect-label='positions.partners']//ng-select")
    public WebElement candidatesPartner;
    @FindBy(xpath = "//app-select[@ng-reflect-label='common.position']//ng-select")
    public WebElement candidatesPosition;
    @FindBy(xpath = "//app-select[@label='common.status']//ng-select")
    public WebElement candidatesStatus;
    @FindBy(xpath = "//app-bulk-edit-confirm-modal[@class='ng-star-inserted']//span[text()='Igen']")
    public WebElement bulkEditConfirmButton;
    @FindBy(xpath = "//button[.//span[text()='Befejezés']]")
    public WebElement bulkEditFinishButton;
    @FindBy(css = "body > app-root > div > section.main-content > main > app-partner-list > div > div > app-table > div > ngx-datatable > div > datatable-body")
    public List<WebElement> records;

    public String keyword = "Do";
    public void candidatesFilter() throws InterruptedException {
        filterButton.click();
        filterCheckbox.click();
        filterContactName.sendKeys(keyword);
        Thread.sleep(1500);
    }
    public boolean candidatesFilterCheck(String keyword){
        for (WebElement element : records) {
            if (!element.getText().contains(keyword)) {
                return false;
            }
        }
        return true;
    }
    public void candidatesFilterDelete(){
        filterClearButton.click();
        filterButton.click();
    }

    public void candidatesFilterSave() throws InterruptedException {
        filterButton.click();
        filterCheckbox.click();
        filterContactName.sendKeys(keyword);
        filterDotMenu.click();
        saveButton.click();
        filterSaveNameField.sendKeys(keyword);
        filterSaveVisibilitySelect.click();
        filterSaveVisibilityPrivate.click();
        Thread.sleep(1000);
        saveButton.click();
        waitForElementToBeVisible(toastMessage);
        myFilters.click();

    }
    public void candidatesFilterByCharacter() throws InterruptedException {
        abcFilter_A.click();
        Thread.sleep(1500);
    }

    public void columnSort() throws InterruptedException {
        waitForElementToBeVisible(columnSort);
        columnSort.click();
        Thread.sleep(1500);
    }


    public void bulkEdit() throws InterruptedException {
        bulkEditCheckbox_1.click();
        bulkEditCheckbox_2.click();
        bulkEditButton.click();
        waitForElementToBeVisible(candidatesMembershipDate);
        candidatesMembershipDate.click();
        waitForElementToBeVisible(dateNumber25);
        dateNumber25.click();
        candidatesPartner.click();
        selectOption_0.click();
        Thread.sleep(1000);
        positionsProject.click();
        selectOption_0.click();
        Thread.sleep(1000);
        candidatesPosition.click();
        selectOption_0.click();
        Thread.sleep(1000);
        candidatesStatus.click();
        selectOption_0.click();
        Thread.sleep(1000);
        yesButton.click();
        bulkEditConfirmButton.click();
        bulkEditFinishButton.click();
    }
    public void createCandidate() {
        createButton.click();
        waitForElementToBeVisible(lastName);
        lastName.sendKeys("/Test");
        firstName.sendKeys("Candidate");
        birthday.click();
        waitForElementToBeVisible(monthYearButton);
        monthYearButton.click();
        year2001.click();
        monthJanuary.click();
        dateNumber10.click();
        email.sendKeys("testcandidate4@gmail.com");
        phoneNumber.sendKeys("+36 309876543");
        source.click();
        selectOption_0.click();
        country.click();
        selectOption_0.click();
        settlement.click();
        settlement.sendKeys("9022");
        selectOption_0.click();
        streetHouse.sendKeys("Test utca 1.");
        newsletter.click();
        selectOption_0.click();
        preferredLanguage.click();
        selectOption_0.click();
        termsNotification.click();
        selectOption_0.click();
        submitSaveButton.click();
    }

}
