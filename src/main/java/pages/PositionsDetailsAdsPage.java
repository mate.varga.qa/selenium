package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PositionsDetailsAdsPage extends BasePage {
    public PositionsDetailsAdsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//span[normalize-space()='hirdetések']")
    public WebElement positionsAdsButton;
    @FindBy(xpath = "//app-datepicker[@formcontrolname='startDate']//button")
    public WebElement positionAdsStartDate;
    @FindBy(xpath = "//app-datepicker[@formcontrolname='endDate']//button")
    public WebElement positionAdsEndDate;
    @FindBy(xpath = "//table[@class='mat-calendar-table']//button[.//div[contains(text(),'15')]]")
    public WebElement positionAdsDateNumber15;
    @FindBy(xpath = "//table[@class='mat-calendar-table']//button[.//div[contains(text(),'17')]]")
    public WebElement positionAdsDateNumber17;
    @FindBy(xpath = "//table[@class='mat-calendar-table']//button[.//div[contains(text(),'20')]]")
    public WebElement positionAdsDateNumber20;
    @FindBy(xpath = "//table[@class='mat-calendar-table']//button[.//div[contains(text(),'22')]]")
    public WebElement positionAdsDateNumber22;
    @FindBy(xpath = "//app-dot-menu[@ng-reflect-menu-items='[object Object],[object Object']//button[@type='button']//*[name()='svg']")
    public WebElement positionsAdsDotMenu;
    @FindBy(xpath = "//button[normalize-space()='Hirdetés törlése']")
    public WebElement positionsAdsDotMenuDeleteOption;
    @FindBy(xpath = "//button[normalize-space()='Hirdetés szerkesztése']")
    public WebElement positionsAdsDotMenuEditOption;

    public void createAd() throws InterruptedException {
        createCrossButton.click();
        projectName.sendKeys("Test Advertisement");
        positionAdsStartDate.click();
        waitForElementToBeVisible(positionAdsDateNumber15);
        positionAdsDateNumber15.click();
        positionAdsEndDate.click();
        waitForElementToBeVisible(positionAdsDateNumber20);
        positionAdsDateNumber20.click();
        partnersContact.click();
        selectOption_0.click();
        Thread.sleep(1000);
        yesButton.click();
    }
    public void deleteAd() {
        positionsAdsDotMenu.click();
        positionsAdsDotMenuDeleteOption.click();
        alternateConfirmButton.click();
    }
    public void editAd() throws InterruptedException {
        positionsAdsDotMenu.click();
        positionsAdsDotMenuEditOption.click();
        positionAdsStartDate.click();
        waitForElementToBeVisible(positionAdsDateNumber17);
        positionAdsDateNumber17.click();
        positionAdsEndDate.click();
        waitForElementToBeVisible(positionAdsDateNumber22);
        positionAdsDateNumber22.click();
        Thread.sleep(1000);
        yesButton.click();
    }
}
