package pages;

import base.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class PositionsPage extends BasePage {
    public PositionsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//input[@placeholder='Pozíció név']")
    public WebElement filterContactName;
    @FindBy(xpath = "//span[contains(text(),'Pozíció neve')]")
    public WebElement columnSort;
    @FindBy(xpath = "//app-select[@label='positions.status']//ng-select")
    public WebElement positionsStatus;
    @FindBy(xpath = "//app-select[@label='candidates.work_type']//ng-select")
    public WebElement positionsWorkType;
    @FindBy(xpath = "//app-select[@label='candidates.physical_work_type']//ng-select")
    public WebElement positionsPhysicalWorkType;
    @FindBy(xpath = "//app-select[@formcontrolname='id']//ng-select")
    public WebElement positionsCategories;
    @FindBy(xpath = "//div[@class='specialization-name']")
    public WebElement selectCategoryOption_0;
    @FindBy(xpath = "//app-select[@ng-reflect-label='common.name']//ng-select")
    public WebElement positionsProjectManager;

    @FindBy(xpath = "//app-select[@label='positions.employment_type']//ng-select")
    public WebElement positionsEmploymentType;
    @FindBy(xpath = "//app-bulk-edit-confirm-modal[@class='ng-star-inserted']//span[text()='Igen']")
    public WebElement bulkEditConfirmButton;
    @FindBy(xpath = "//button[.//span[text()='Befejezés']]")
    public WebElement bulkEditFinishButton;
    @FindBy(css = "body > app-root > div > section.main-content > main > app-partner-list > div > div > app-table > div > ngx-datatable > div > datatable-body")
    public List<WebElement> records;

    public String keyword = "Gép";
    public void positionsFilter() throws InterruptedException {
        filterButton.click();
        filterCheckbox.click();
        filterContactName.sendKeys(keyword);
        Thread.sleep(1500);
    }
    public boolean positionsFilterCheck(String keyword){
        for (WebElement element : records) {
            if (!element.getText().contains(keyword)) {
                return false;
            }
        }
        return true;
    }
    public void positionsFilterDelete(){
        filterClearButton.click();
        filterButton.click();
    }

    public void positionsFilterSave() throws InterruptedException {
        filterButton.click();
        filterCheckbox.click();
        filterContactName.sendKeys(keyword);
        filterDotMenu.click();
        saveButton.click();
        filterSaveNameField.sendKeys(keyword);
        filterSaveVisibilitySelect.click();
        filterSaveVisibilityPrivate.click();
        Thread.sleep(1000);
        saveButton.click();
        waitForElementToBeVisible(toastMessage);
        myFilters.click();

    }
    public void positionsFilterByCharacter() throws InterruptedException {
        Thread.sleep(1000);
        waitForElementToBeVisible(abcFilter_A);
        abcFilter_A.click();
        Thread.sleep(1500);
    }

    public void columnSort() throws InterruptedException {
        waitForElementToBeVisible(columnSort);
        columnSort.click();
        Thread.sleep(1500);
    }
    public void bulkEdit() throws InterruptedException {
        bulkEditCheckbox_1.click();
        bulkEditCheckbox_2.click();
        bulkEditButton.click();
        waitForElementToBeVisible(positionsPartner);
        positionsPartner.click();
        selectOption_0.click();
        Thread.sleep(1000);
        positionsProject.click();
        selectOption_0.click();
        positionsStatus.click();
        selectOption_0.click();
        positionsWorkType.click();
        selectOption_0.click();
        positionsPhysicalWorkType.click();
        selectOption_0.click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", positionsCategories);
        positionsCategories.click();
        selectCategoryOption_0.click();
        Thread.sleep(1000);
        yesButton.click();
        bulkEditConfirmButton.click();
        bulkEditFinishButton.click();
    }
    public void createPosition() throws InterruptedException {
        createButton.click();
        waitForElementToBeVisible(projectName);
        projectName.sendKeys("Test Position");
        partnersName.click();
        selectOption_0.click();
        positionsProject2.click();
        selectOption_0.click();
        positionsProjectManager.click();
        selectOption_0.click();
        positionsStatus.click();
        selectOption_0.click();
        positionsWorkType.click();
        selectOption_0.click();
        positionsPhysicalWorkType.click();
        selectOption_0.click();
        positionsCategories.click();
        selectCategoryOption_0.click();
        positionsEmploymentType.click();
        selectOption_0.click();
        Thread.sleep(1000);
        submitSaveButton.click();
    }

}
