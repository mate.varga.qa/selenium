package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ManageUsersPage extends BasePage {
    public ManageUsersPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//a[@class='ng-star-inserted']")
    public WebElement manageUsersButton;
    @FindBy(xpath = "//app-select[@label='users.role']//ng-select")
    public WebElement usersRole;
    @FindBy(xpath = "//tbody/tr[1]/td[6]/div[1]/app-dot-menu[1]/button[1]//*[name()='svg']//*[name()='path' and contains(@d,'M2 4.96875')]")
    public WebElement firstUserDotMenu;



    public void createUser() throws InterruptedException {
        createCrossButton.click();
        lastName.sendKeys("!!Test");
        firstName.sendKeys("Manager");
        email.sendKeys("manager@gmail.com");
        phoneNumber.sendKeys("+36 201234567");
        partnersOffice.click();
        selectOption_0.click();
        usersRole.click();
        selectOption_0.click();
        Thread.sleep(1000);
        saveButton.click();
    }
    public void deleteUser() {
        firstUserDotMenu.click();
        deleteButton.click();
        alternateConfirmButton.click();
    }
    public void editUser() throws InterruptedException {
        firstUserDotMenu.click();
        dotMenuEditButton.click();
        lastName.clear();
        lastName.sendKeys("!!Test Edit");
        firstName.clear();
        firstName.sendKeys("Edit User");
        Thread.sleep(1000);
        saveButton.click();
    }
}
