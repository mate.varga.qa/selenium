package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PositionsDetailsKanbanPage extends BasePage {
    public PositionsDetailsKanbanPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//span[normalize-space()='jelöltek']")
    public WebElement positionsKanbanButton;
    @FindBy(xpath = "//span[@class='ng-arrow-wrapper']")
    public static WebElement kanbanSortArrow;
    @FindBy(xpath = "//app-fab-button[@class='view-switcher-button icon pr-3']//button")
    public static WebElement kanbanViewButton;
    @FindBy(xpath = "//app-primary-button[@type='submit']//span[@class='primary-button-text-value']")
    public WebElement createKanbanCandidateButton;

    public void sortKanban() {
        kanbanSortButton.click();
        selectOption_0.click();
    }
    public void changeKanbanView() throws InterruptedException {
        kanbanViewButton.click();
        Thread.sleep(1500);
        kanbanViewButton.click();
    }
    public void createKanbanCandidate() {
        createKanbanCandidateButton.click();
        lastName.sendKeys("Test");
        firstName.sendKeys("Kanban");
        birthday.click();
        waitForElementToBeVisible(monthYearButton);
        monthYearButton.click();
        year2001.click();
        monthJanuary.click();
        dateNumber10.click();
        email.sendKeys("testkanban@gmail.com");
        phoneNumber.sendKeys("+36 301234567");
        source.click();
        selectOption_0.click();
        country.click();
        selectOption_0.click();
        settlement.click();
        settlement.sendKeys("9022");
        selectOption_0.click();
        streetHouse.sendKeys("Test utca 1.");
        newsletter.click();
        selectOption_0.click();
        preferredLanguage.click();
        selectOption_0.click();
        termsNotification.click();
        selectOption_0.click();
        submitSaveButton.click();
    }
}
