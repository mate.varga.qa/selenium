package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PositionsDetailsPage extends BasePage {
    public PositionsDetailsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//app-key-value-wrapper[@class='ng-star-inserted']//button[@class='key-value-header-icon']")
    public WebElement editPartnerButton;

    public void writeHistoryMessage(String message) {
        historyMessage.sendKeys(message);
        saveButton.click();
    }
    public void showFullHistory() throws InterruptedException {
        showFullHistory.click();
        Thread.sleep(1500);
        hideFullHistory.click();
    }
    public void editPosition() {
        editPartnerButton.click();
        waitForElementToBeVisible(projectName);
        projectName.clear();
        projectName.sendKeys("Test Position Edit");
        submitSaveButton.click();
    }
    public void revertEditPosition() {
        editPartnerButton.click();
        waitForElementToBeVisible(projectName);
        projectName.clear();
        projectName.sendKeys("Almaszedő");
        submitSaveButton.click();
    }
}
