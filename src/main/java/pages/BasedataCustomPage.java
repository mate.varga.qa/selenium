package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BasedataCustomPage extends BasePage{
    public BasedataCustomPage(WebDriver driver) {
            super(driver);
    }
    @FindBy(xpath = "//span[normalize-space()='dinamikus adatok']")
    public static WebElement dynamicFieldsButton;
    @FindBy(xpath = "//app-text-field[@ng-reflect-placeholder='base_data.sources.search']//input")
    public WebElement sourceSearch;
    @FindBy(xpath = "//span[contains(text(),'Típus')]")
    public WebElement columnSort;
    @FindBy(xpath = "//datatable-body-cell[@role='cell']//app-dot-menu[@class='justify-content-center d-flex ng-star-inserted']//button")
    public WebElement customDotMenu;
    @FindBy(xpath = "//span[contains(text(),'Név')]")
    public WebElement customColumnName;
    @FindBy(xpath = "//span[@title='Egyéb végzettség']")
    public WebElement sourceOther;
    @FindBy(xpath = "//app-select[@formcontrolname='type']//ng-select")
    public WebElement sourceType;

    public void searchCustomField() throws InterruptedException {
        waitForElementToBeVisible(sourceSearch);
        sourceSearch.sendKeys("egy");
        Thread.sleep(1000);
    }
    public void sortCustomField() throws InterruptedException {
        columnSort.click();
        Thread.sleep(1000);
        columnSort.click();
        Thread.sleep(1000);
        columnSort.click();
    }
    public void editCustomField() {
        customDotMenu.click();
        dotMenuEditButton.click();
        projectName.sendKeys(" - Test");
        saveButton.click();
    }
    public void revertEditCustomField() {
        customDotMenu.click();
        dotMenuEditButton.click();
        projectName.clear();
        projectName.sendKeys("Targoncás jogosítvány");
        saveButton.click();
    }
    public void createCustomField() throws InterruptedException {
        createCrossButton.click();
        projectName.sendKeys("!Test CustomField");
        sourceType.click();
        selectOption_0.click();
        Thread.sleep(1000);
        saveButton.click();
    }
    public void deleteCustomField() {
        customColumnName.click();
        customDotMenu.click();
        waitForElementToBeVisible(deleteButton);
        deleteButton.click();
        alternateConfirmButton.click();
    }
}
