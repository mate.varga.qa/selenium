package pages;

import base.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PartnersDetailsPage extends BasePage {
    public PartnersDetailsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//datatable-row-wrapper[3]//datatable-body-cell[.//span[contains(text(), 'Agrolab Europe')]]")
    public WebElement firstPartner;
    @FindBy(xpath = "//textarea[@placeholder='Jegyzet írása...']")
    public WebElement historyMessage;
    @FindBy(xpath = "//button[normalize-space()='Teljes történet mutatása']")
    public WebElement showFullHistory;
    @FindBy(xpath = "//button[normalize-space()='Megjegyzések mutatása']")
    public WebElement hideFullHistory;
    @FindBy(xpath = "//*[name()='path' and contains(@d,'M10.875 4.')]")
    public WebElement createProjectButton;
    @FindBy(xpath = "//app-text-field[@formcontrolname='identifier']//input")
    public WebElement projectID;
    @FindBy(xpath = "//app-select[@formcontrolname='status']")
    public WebElement projectStatus;
    @FindBy(xpath = "//app-select[@ng-reflect-label='common.name']/ng-select")
    public WebElement projectManager;
    @FindBy(xpath = "//tbody/tr[1]/td[4]/div[1]/app-dot-menu[1]/button[1]//*[name()='svg']//*[name()='path' and contains(@d,'M2 4.96875')]")
    public WebElement projectDotMenu;
    @FindBy(xpath = "//button[normalize-space()='Projekt törlése']")
    public WebElement deleteProjectButton;


    public void switchToggle() {
        switchButton.click();
        yesButton.click();
    }
    public void writeHistoryMessage(String message) {
        historyMessage.sendKeys(message);
        saveButton.click();
    }
    public void showFullHistory() throws InterruptedException {
        showFullHistory.click();
        Thread.sleep(1500);
        hideFullHistory.click();
    }
    public void createProject() throws InterruptedException {
        createProjectButton.click();
        projectName.sendKeys("!Teszt Projekt");
        projectID.sendKeys("TP01");
        partnersOffice.click();
        selectOption_0.click();
        projectStatus.click();
        selectOption_0.click();
        country.click();
        selectOption_0.click();
        settlement.click();
        settlement.sendKeys("9022");
        selectOption_0.click();
        streetHouse.sendKeys("Test utca 1.");
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", projectManager);
        projectManager.click();
        selectOption_0.click();
        partnersContact.click();
        selectOption_0.click();
        Thread.sleep(1000);
        submitSaveButton.click();
    }
    public void deleteProject() {
        projectDotMenu.click();
        deleteProjectButton.click();
        alternateConfirmButton.click();
    }
    public void editPartner() {
        editPartnerButton.click();
        waitForElementToBeVisible(partnersCompanyName);
        partnersCompanyName.clear();
        partnersCompanyName.sendKeys("Test Edit");
        partnersTaxNumber.clear();
        partnersTaxNumber.sendKeys("97643128-5-00");
        submitSaveButton.click();
    }
    public void revertEditPartner() {
        editPartnerButton.click();
        waitForElementToBeVisible(partnersCompanyName);
        partnersCompanyName.clear();
        partnersCompanyName.sendKeys("Agrolab Europe");
        submitSaveButton.click();
    }
}
